import PackageDescription

let package = Package(
    name: "APIXU-Swift",
    dependencies: [
        .Package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", majorVersion: 3)
    ]
)
