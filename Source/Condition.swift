struct Condition{

    public var text: String
    public var icon: String
    public var code: Int
            
    init(text: String, icon: String, code: Int){
        self.text = text
        self.icon = icon
        self.code = code
    }
}
