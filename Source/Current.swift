extension APIXU{
    
    struct Current {
        
        public var last_updated         : String
        public var last_updated_epoch   : Int
        public var temp_c               : Double
        public var temp_f               : Double
        public var feelslike_c          : Double
        public var feelsline_f          : Double
        public var condition            : Condition
        public var wind_mph             : Double
        public var wind_kph             : Double
        public var wind_degree          : Int
        public var wind_dir             : String
        public var pressure_mb          : Double
        public var pressure_in          : Double
        public var precip_mm            : Double
        public var precip_in            : Double
        public var humidity             : Int
        public var cloud                : Int
        public var is_day               : Int
        public var vis_km               : Double
        public var vis_miles            : Double

    }
    
}
