extension APIXU {
    

    struct  Error {
        public var code: Int
        public var message: String
        
        init(code: Int, message: String) {
            self.code    = code
            self.message = message
        }
    }
}
