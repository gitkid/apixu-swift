extension APIXU {
    
    struct Forecast {
    
        var ForecastDay: [ForecastDay] = []
        
        struct ForecastDay {
            public var date:        String
            public var data_epoch:  Int
            public var days:        ForecastDay.Day
            public var astro:       ForecastDay.Astro
            public var hour:        [ForecastDay.Hour]
            
            struct Day {
                
                public var maxtemp_c: Double
                public var maxtemp_f: Double
                public var mintemp_c: Double
                public var mintemp_f: Double
                public var avgtemp_c: Double
                public var avgtemp_f: Double
                public var maxwind_mph: Double
                public var maxwind_kph: Double
                public var totalprecip_mm: Double
                public var totalprecip_in: Double
                public var avgvis_km: Double
                public var avgvis_miles: Double
                public var avghumidity: Int
                public var condition: Condition
            }
            
            struct Astro {
                
                public var sunrise: String
                public var sunset: String
                public var moonrise: String
                public var moonset: String
            }
            
            struct Hour {
                
                public var time_epoch: Int
                public var time: String
                public var temp_c: Double
                public var temp_f: Double
                public var condition: Condition
                public var wind_mph: Double
                public var wind_kph: Double
                public var wind_degree: Int
                public var wind_dir: String
                public var pressure_mb: Double
                public var pressure_in: Double
                public var precip_mm: Double
                public var humidity: Int
                public var cloud: Int
                public var feelslike_c: Double
                public var feelslike_f: Double
                public var windchill_c: Double
                public var windchill_f: Double
                public var heatindex_f: Double
                public var heatindex_c: Double
                public var dewpoint_c: Double
                public var dewpoint_f: Double
                public var will_it_rain: Int
                public var will_it_snow: Int
                public var is_day: Int
                public var vis_km: Double
                public var vis_miles: Double
                
            }
        }
    }
}
