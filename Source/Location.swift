extension APIXU {
    
    struct Location {
        public var name: String
        public var region: String
        public var country: String
        public var lat: Double
        public var lon: Double
        public var tz_id: String
        public var localtime_epoch: Int
        public var localtime: String
        
        init(name: String, region: String, country: String, lat: Double, lon: Double,
             tz_id: String, localtime_epoch: Int, localtime: String) {
            self.name            = name
            self.region          = region
            self.country         = country
            self.lat             = lat
            self.lon             = lon
            self.tz_id           = tz_id
            self.localtime_epoch = localtime_epoch
            self.localtime       = localtime
        }
    }
}
