import Foundation
import SwiftyJSON

class APIXU {
    // User token
    private let token: String
    
    // URL (change if needed)
    public  let apiuxURL = "http://api.apixu.com/v1/"
    
    init(token: String){
        self.token = token
    }
    
    enum APIMethod : String {
        case current
        case forecast
        case history
    }
    
    public func history(_ q: String = "auto:ip", dt: String, copmletion: @escaping (Location?, Current?, Forecast?, Error?)->()){
        var params = [String:String]()
        params["q"] = q
        params["dt"] = dt
        
        self.request(.history, params: params){
            data in
            var returnLocation  : Location?
            var returnCurrent   : Current?
            var returnForecast  : Forecast?
            var returnError     : Error?
            
            
            let json = JSON(data: data)
            
            let error = json["error"].dictionaryValue
            if !error.isEmpty {
                returnError = self.getError(error)
            }
            
            let location = json["location"].dictionaryValue
            if !location.isEmpty {
                returnLocation = self.getLocation(location)
            }
            
            let current = json["current"].dictionaryValue
            if !current.isEmpty {
                returnCurrent = self.getCurrent(current)
            }
            
            let forecast = json["forecast"].dictionaryValue
            if !forecast.isEmpty{
                returnForecast = self.getForecast(forecast)
            }
            copmletion(returnLocation, returnCurrent, returnForecast, returnError)
        }
    }
    
    public func forecast(_ q: String = "auto:ip", days: Int, copmletion: @escaping (Location?, Current?, Forecast?, Error?)->()){
        var params = [String:String]()
        params["q"] = q
        params["days"] = String(days)
        
        self.request(.forecast, params: params){
            data in
            var returnLocation  : Location?
            var returnCurrent   : Current?
            var returnForecast  : Forecast?
            var returnError     : Error?
            
            
            let json = JSON(data: data)
            
            let error = json["error"].dictionaryValue
            if !error.isEmpty {
                returnError = self.getError(error)
            }
            
            let location = json["location"].dictionaryValue
            if !location.isEmpty {
                returnLocation = self.getLocation(location)
            }
            
            let current = json["current"].dictionaryValue
            if !current.isEmpty {
                returnCurrent = self.getCurrent(current)
            }
            
            let forecast = json["forecast"].dictionaryValue
            if !forecast.isEmpty{
                returnForecast = self.getForecast(forecast)
            }
            copmletion(returnLocation, returnCurrent, returnForecast, returnError)
        }
    }
    
    public func current(_ q: String = "auto:ip", completion: @escaping (Location?, Current?, Error?)->()){
        var params = [String:String]()
        params["q"] = q
        
        self.request(.current, params: params){
            data in
            var returnError:     Error?
            var returnLocation:  Location?
            var returnCurrent:   Current?
            
            let json = JSON(data: data)
            
            let error = json["error"].dictionaryValue
            if !error.isEmpty {
                returnError = self.getError(error)
            }
            
            let location = json["location"].dictionaryValue
            if !location.isEmpty {
                returnLocation = self.getLocation(location)
            }
            
            let current = json["current"].dictionaryValue
            if !current.isEmpty {
               returnCurrent = self.getCurrent(current)
            }
            
            completion(returnLocation, returnCurrent, returnError)
            
        }
    }
    
    private func getLocation(_ location: [String: JSON])->Location {
        let returnLocation: Location
        returnLocation = Location(name: location["name"]!.stringValue,                      region: location["region"]!.stringValue,
                                  country: location["country"]!.stringValue,                lat: location["lat"]!.doubleValue,
                                  lon: location["lon"]!.doubleValue,                        tz_id: location["tz_id"]!.stringValue,
                                  localtime_epoch: location["localtime_epoch"]!.intValue,   localtime: location["localtime"]!.stringValue)
        return returnLocation
    }
    
    private func getCurrent(_ current: [String: JSON])->Current {
        let returnCurrent: Current
        let returnCondition = getCondition(current["condition"]!.dictionaryValue)
        
        
        returnCurrent = Current(last_updated: current["last_updated"]!.stringValue, last_updated_epoch: current["last_updated_epoch"]!.intValue,
                                temp_c: current["temp_c"]!.doubleValue,             temp_f: current["temp_f"]!.doubleValue,
                                feelslike_c: current["feelslike_c"]!.doubleValue,   feelsline_f: current["feelslike_f"]!.doubleValue,
                                condition: returnCondition,                         wind_mph: current["wind_mph"]!.doubleValue,
                                wind_kph: current["wind_kph"]!.doubleValue,         wind_degree: current["wind_degree"]!.intValue,
                                wind_dir: current["wind_dir"]!.stringValue,         pressure_mb: current["pressure_mb"]!.doubleValue,
                                pressure_in: current["pressure_in"]!.doubleValue,   precip_mm: current["precip_mm"]!.doubleValue,
                                precip_in: current["precip_in"]!.doubleValue,       humidity: current["humidity"]!.intValue,
                                cloud: current["cloud"]!.intValue,                  is_day: current["is_day"]!.intValue,
                                vis_km: current["vis_km"]!.doubleValue,             vis_miles: current["vis_miles"]!.doubleValue)
        return returnCurrent
    }
    
    private func getCondition(_ condition: [String: JSON])->Condition{
        let returnCondition: Condition
        returnCondition = Condition(text: condition["text"]!.stringValue,
                                    icon: condition["icon"]!.stringValue,
                                    code: condition["code"]!.intValue)
        return returnCondition
    }
    
    private func getError(_ error: [String: JSON])-> Error{
        let returnError: Error
        returnError = Error(code    : error["code"]!.intValue,
                            message : error["message"]!.stringValue)
        return returnError
    }
    
    private func getForecast(_ forecast: [String: JSON])->Forecast {
        var returnForecast = Forecast()
        let forecastDay = forecast["forecastday"]!.arrayValue
        for day in forecastDay{
            returnForecast.ForecastDay.append(getForecastDay(day: day))
        }
        return returnForecast
    }
    
    private func getForecastDay(day: JSON)->Forecast.ForecastDay {
        let returnForecastDay   : Forecast.ForecastDay
        let returnDay           : Forecast.ForecastDay.Day
        let returnAstro         : Forecast.ForecastDay.Astro
        var returnHour          : Array<Forecast.ForecastDay.Hour> = []
        
        returnDay = getDay(day: day["day"].dictionaryValue)
        returnAstro = getAstro(astro: day["astro"].dictionaryValue)
        for hour in day["hour"].arrayValue{
            returnHour.append(self.getHour(hour: hour.dictionaryValue))
        }
        
        returnForecastDay = Forecast.ForecastDay(date: day["date"].stringValue, data_epoch: day["day_epoch"].intValue,
                                                 days: returnDay, astro: returnAstro, hour: returnHour)
        return returnForecastDay
    }
    
    private func getDay(day: [String: JSON])-> Forecast.ForecastDay.Day{
        let condition = self.getCondition(day["condition"]!.dictionaryValue)
        let returnDay = Forecast.ForecastDay.Day(maxtemp_c: day["maxtemp_c"]!.doubleValue,              maxtemp_f: day["maxtemp_f"]!.doubleValue,
                                                 mintemp_c: day["mintemp_c"]!.doubleValue,              mintemp_f: day["mintemp_f"]!.doubleValue,
                                                 avgtemp_c: day["avgtemp_c"]!.doubleValue,              avgtemp_f: day["avgtemp_f"]!.doubleValue,
                                                 maxwind_mph: day["maxwind_mph"]!.doubleValue,          maxwind_kph: day["maxwind_kph"]!.doubleValue,
                                                 totalprecip_mm: day["totalprecip_mm"]!.doubleValue,    totalprecip_in: day["totalprecip_in"]!.doubleValue,
                                                 avgvis_km: day["avgvis_km"]!.doubleValue,              avgvis_miles: day["avgvis_miles"]!.doubleValue,
                                                 avghumidity: day["avghumidity"]!.intValue,             condition: condition)
        return returnDay
    }
    
    private func getAstro(astro: [String: JSON])-> Forecast.ForecastDay.Astro{
        let returnAstro = Forecast.ForecastDay.Astro(sunrise: astro["sunrise"]!.stringValue, sunset: astro["sunset"]!.stringValue,
                                                     moonrise: astro["moonrise"]!.stringValue, moonset: astro["moonset"]!.stringValue)
        return returnAstro
    }
    
    private func getHour(hour: [String: JSON])-> Forecast.ForecastDay.Hour{
        let condition = self.getCondition(hour["condition"]!.dictionaryValue)
        let returnHour = Forecast.ForecastDay.Hour(time_epoch: hour["time_epoch"]!.intValue,        time: hour["time"]!.stringValue,
                                                   temp_c: hour["temp_c"]!.doubleValue,             temp_f: hour["temp_f"]!.doubleValue,
                                                   condition: condition,                            wind_mph: hour["wind_mph"]!.doubleValue,
                                                   wind_kph: hour["wind_kph"]!.doubleValue,         wind_degree: hour["wind_degree"]!.intValue,
                                                   wind_dir: hour["wind_dir"]!.stringValue,         pressure_mb: hour["pressure_mb"]!.doubleValue,
                                                   pressure_in: hour["pressure_in"]!.doubleValue,   precip_mm: hour["precip_mm"]!.doubleValue,
                                                   humidity: hour["humidity"]!.intValue,            cloud: hour["cloud"]!.intValue,
                                                   feelslike_c: hour["feelslike_c"]!.doubleValue,   feelslike_f: hour["feelslike_f"]!.doubleValue,
                                                   windchill_c: hour["windchill_c"]!.doubleValue,   windchill_f: hour["windchill_f"]!.doubleValue,
                                                   heatindex_f: hour["heatindex_f"]!.doubleValue,   heatindex_c: hour["heatindex_c"]!.doubleValue,
                                                   dewpoint_c: hour["dewpoint_c"]!.doubleValue,     dewpoint_f: hour["dewpoint_f"]!.doubleValue,
                                                   will_it_rain: hour["will_it_rain"]!.intValue,    will_it_snow: hour["will_it_snow"]!.intValue,
                                                   is_day: hour["is_day"]!.intValue,                vis_km: hour["vis_km"]!.doubleValue,
                                                   vis_miles: hour["vis_miles"]!.doubleValue)
        return returnHour
    }
    
    private func request(_ method: APIMethod, params: Dictionary<String,String>, completion: @escaping (Data)->()){
        var link = apiuxURL + method.rawValue + ".json?"
        link += "key=" + self.token + "&"
        for (index,param) in params.enumerated() {
            link += param.key + "=" + param.value
            if index != params.count-1 {link += "&"}
        }
        
        let url = URL(string: link)
        let task = URLSession.shared.dataTask(with: url!){
            (data, responce, error) in
            guard error == nil else {print(error!);return}
            
            completion(data!)
        }
        task.resume()
        
    }
}


// Example

let api = APIXU(token: "3a1046f867794c06908114620171602")
api.forecast("Moscow", days: 5){
    location,current,forecast,error in
    guard error == nil else {print(error!.message);return}
    
    for day in forecast!.ForecastDay{
        print("Date: \(day.date) maxtemp_c: \(day.days.maxtemp_c) C")
    }
}
sleep(5)
